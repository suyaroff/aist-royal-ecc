class App
{
    welcomeVideo
    welcomeAuthor
    overLayer
    overLayerBox
    childCount = 1
    childAge = { junior: false, middle: false, senior: false }
    wizardStep = 1
    wizard
    wizard_1 = document.querySelector('.wizard__step_1')
    wizard_2 = document.querySelector('.wizard__step_2')
    wizard_3 = document.querySelector('.wizard__step_3')
    customer = { name: '', phone: '' }

    showVideo ()
    {
        this.welcomeVideo.innerHTML = `<iframe
        src="https://www.youtube.com/embed/AMLWvTklvHs?autoplay=1"
        title="YouTube video player"
        frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen></iframe>`
    }

    checkBoxHandler ()
    {
        document.querySelectorAll(".c-checkbox").forEach((checkBox) =>
        {
            this.childAge[checkBox.value] = checkBox.checked
            checkBox.addEventListener('change', (e) =>
            {
                this.setAge(e.target)
            })
        })
    }
    setAge (el)
    {
        this.childAge[el.value] = el.checked
    }
    checkAge ()
    {
        for (const age in this.childAge) {
            if (this.childAge[age]) {
                return this.childAge[age]
            }
        }
        return false
    }
    overlayerShow ()
    {
        this.overLayer.style.display = "flex"
    }
    overlayerHide ()
    {
        this.overLayer.style.display = "none"
        this.overLayerBody.innerHTML = ""
    }
    signUpShow ()
    {
        this.overlayerShow()
        this.overLayerBody.appendChild(this.wizard)
        this.wizard.style.display = 'block'
    }
    setChildCount (num)
    {
        num = parseInt(num)
        const childCountField = document.querySelector('.count-field')
        if (num >= 1 && num <= 10) {
            this.childCount = num
            childCountField.value = this.childCount
            return true
        } else {
            childCountField.value = 1
            return false
        }
    }
    childPlus ()
    {
        this.setChildCount(this.childCount + 1)
    }
    childMinus ()
    {
        this.setChildCount(this.childCount - 1)
    }
    nextStep (stepNum)
    {
        stepNum = parseInt(stepNum)
        if (stepNum == 2) {

            if (this.childCount >= 1 && this.childCount <= 10) {
                this.wizard_1.style.display = 'none'
                this.wizard_2.style.display = 'flex'
            } else {
                alert('Set number of children')
            }
        }
        if (stepNum == 3) {
            if (this.checkAge()) {
                this.wizard_2.style.display = 'none'
                this.wizard_3.style.display = 'flex'
            } else {
                alert('Select baby age')
            }

        }
        if (stepNum == 4) {
            this.customer.name = document.querySelector('.field_name').value
            this.customer.phone = document.querySelector('.field_phone').value
            if (this.customer.name && this.customer.phone) {
                const body = JSON.stringify({
                    child: { count: this.childCount, age: this.childAge },
                    customer: this.customer
                })
                fetch('/lead.json', {
                    method: 'GET',
                    headers: { 'Content-Type': 'application/json' }
                }).then((responce) => { return responce.json() }).then((data) =>
                {
                    if (data.success) {
                        this.wizard_3.style.display = 'none'
                        document.querySelector('.wizard__success').style.display = 'flex'
                    } else {
                        alert('Sent error')
                    }
                })

            } else {
                alert('Set name and phone')
            }
        }
    }
    backStep (stepNum)
    {
        stepNum = parseInt(stepNum)
        if (stepNum == 1) {
            this.wizard_1.style.display = 'flex'
            this.wizard_2.style.display = 'none'
        }
        if (stepNum == 2) {
            this.wizard_2.style.display = 'flex'
            this.wizard_3.style.display = 'none'
        }

    }
    init ()
    {
        console.log("Dom Loaded")
        this.welcomeVideo = document.querySelector(".welcome__video")
        this.welcomeAuthor = document.querySelector(".welcome__author")
        this.overLayer = document.querySelector(".overlayer")
        this.overLayerBody = document.querySelector(".overlayer__body")
        this.wizard = document.querySelector(".wizard")

        if (this.welcomeAuthor) {
            this.welcomeAuthor.addEventListener("click", () =>
            {
                this.showVideo()
            })
        }

        document.querySelector(".overlayer__close").addEventListener("click", () =>
        {
            this.overlayerHide()
        })

        this.checkBoxHandler()

    }
    ready ()
    {
        console.log("Page Loaded")
        document.querySelectorAll(".button_sign-up").forEach((button) =>
        {
            button.addEventListener("click", () =>
            {
                this.signUpShow()
            })
        })
        try {
            const swiper = new Swiper(".swiper", {
                loop: true,
                pagination: {
                    el: ".swiper-pagination",
                },
            })
        } catch (e) { }
    }
}

let app = new App()

document.addEventListener("DOMContentLoaded", app.init())
window.addEventListener("load", app.ready())
